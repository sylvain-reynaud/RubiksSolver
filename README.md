## Présentation

Programme de résolution de Rubik's Cube 3x3x3 avec visuel 3D.

## Vidéo du programme
[![Vidéo du programme](https://img.youtube.com/vi/lXtXKnSMPq8/0.jpg)](https://www.youtube.com/watch?v=lXtXKnSMPq8)

## Installation

- Installer Python 2.7.9
- Installer [VisualPython](http://vpython.org/index.html) pour Python 2.7.9

## Plus d'informations

Sur une moyenne de 10 000 résolutions, le cube est fini en 180 mouvements.

## Remerciements

Merci au blogger [possiblywrong](https://possiblywrong.wordpress.com/2011/01/15/yet-another-rubiks-cube/) pour le bout de code qui a permis la représentation 3D.

## Contributeurs

- Syl20 (Sylvain R)